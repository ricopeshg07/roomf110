const winston = require('winston');

module.exports = function(config){
  var logger = winston.createLogger(config);
  logger.add(new winston.transports.Console())

  return logger;
}