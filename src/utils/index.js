//@flow

export function sleep(ms: number): Promise<void> {
  return new Promise(() => {
    setTimeout(() => {}), ms;
  });
}

export function IsValidJSON(str: string): boolean {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
export function assignOnlyIntersection(sourceObj: Object, targetObj: Object): Object {
  return Object.keys(targetObj).reduce((intersection, key) => {
    intersection[key] =
      typeof targetObj[key] === "object"
        ? assignOnlyIntersection(sourceObj[key], targetObj[key])
        : sourceObj[key] || targetObj[key];
    return intersection;
  }, {});
}
