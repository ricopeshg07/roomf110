//@flow
var awsIot = require("aws-iot-device-sdk");
import { sleep } from "./utils";
import { startThings } from "./things";

async function main() {
  startThings();
  while (1) {
    await sleep(4000);
    console.log("main script running");
  }
}

main();
