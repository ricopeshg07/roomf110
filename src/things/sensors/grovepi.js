//@flow
import Thing from "../thing.js";
var GrovePi = require("node-grovepi").GrovePi;
import { sleep } from "../../utils";
var Commands = GrovePi.commands;
var Board = GrovePi.board;

const MqttPresenceRubrique = ["roomf110", "features", "presence"];

//abstract grovepi sensor class
export default class GrovePiSensors extends Thing {
  //any grovepiSensors in node-grovePi
  sensorList: { type: string, pin: string, watchInterval: string };
  presenceDetectionCM: number; //cm
  constructor(id: string, config: any, options: Object) {
    super(id, config, options);
    this.sensorList = options.sensorList;
    this.presenceDetectionDistance = options.detectionDistance;
    this.detectionOn = true;
    this.detection_settings = {
      START_DETECTION_TIME_H: 8,
      STOP_DETECTION_TIME_H: 18,
      IGNORE_DETECTION_INTERVAL_SECONDS: 300
    };
    if (options.detection_settings)
      this.detection_settings = Object.assign(this.detection_settings, options.detection_settings);
  }

  run(callback: any) {
    var self = this;
    this.connect(() => {
      this.setupBoardAndSensors();
    });
  }

  setupBoardAndSensors() {
    const self = this;
    const board = new Board({
      debug: true,
      onError: function(err) {
        self.logger.error(err);
      },
      onInit: function(res) {
        if (res) {
          self.logger.info(self.id + "starting watch");
          for (let sensorConfig of self.sensorList) {
            let sensor;
            switch (sensorConfig.type) {
              case "DHTDigital":
                const DHTDigitalSensor = GrovePi.sensors.DHTDigital;
                sensor = new DHTDigitalSensor(
                  sensorConfig.pin,
                  DHTDigitalSensor.VERSION.DHT11,
                  DHTDigitalSensor.CELSIUS
                );
                sensor.on("change", function(res: number[]) {
                  // res[0] -> temperature °C, res[1] -> humidity %
                  if (
                    res[0] >= 0 &&
                    res[0] <= 50 &&
                    (res[1] >= 20 && res[1] <= 90)
                  ) {
                    const dhtRes = {
                      temperature: res[0].toString(),
                      humidity: res[1].toString()
                    };
                    self.logger.info(
                      sensorConfig.type + " is reading ",
                      dhtRes
                    );
                    self.update(Object.assign(self.state, dhtRes));
                  }
                });
                break;
              case "UltrasonicDigital":
                const UltrasonicSensor = GrovePi.sensors.UltrasonicDigital;
                sensor = new UltrasonicSensor(sensorConfig.pin);
                sensor.on("change", function(res: number) {
                  const now = new Date();
                  // ignore if not between START AND STOP TIME
                  self.detectionOn =
                    now.getHours() >=
                      self.detection_settings.START_DETECTION_TIME_H &&
                    now.getHours() < self.detection_settings.STOP_DETECTION_TIME_H;
                  if (
                    self.detectionOn &&
                    res <= self.presenceDetectionDistance
                  ) {
                    self.logger.info(
                      sensorConfig.type +
                        " is detecting a presence at " +
                        res +
                        "cm"
                    );
                    const nowTimestamp = +now;
                    const presenceRes = {
                      value: res,
                      timestamp: now
                    };
                    self.send(MqttPresenceRubrique, presenceRes);
                    // Ignore presence during interval
                    self.detectionOn = false;
                    sleep(
                      self.detection_settings.IGNORE_DETECTION_INTERVAL_SECONDS *
                        1000
                    ).then(() => {
                      self.detectionOn = true;
                    });
                  }
                });
                break;
              default:
                sensor = new GrovePi.sensors[sensorConfig.id](sensorConfig.pin);
                sensor.on("change", function(res) {
                  self.logger.info(sensorConfig.type + " is reading ", res);
                });
            }
            sensor.watch(sensorConfig.watchInterval);
          }
        }
      }
    });
    board.init();
  }
}
