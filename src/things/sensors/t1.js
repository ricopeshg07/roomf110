//@flow
import Thing from "../thing.js";
var cron = require("node-cron");

export default class T1 extends Thing {
  run(callback: any) {
    console.log('thing running')
    var self = this;
    this.connect(() => {
      var i = 0;
      var task = cron.schedule("* * * * *", () => {
        i++;
        self.send("test", "test " + i);

        console.log("test sent");
      });
      task.start();
    });
  }
}
