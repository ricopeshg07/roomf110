//@flow

var child_process = require("child_process");
var awsIotNetworkConfig = require("../../network/networkConfig.json");
import Thing from "./thing.js";

/** things Manifests
 * List all the objects related to this device and the arguments to instantiate them
 */
const thingsManifest = require("../../thingsManifest.json");

export function startThings(): { [Thing]: any } {
  const thingsConfig: any[] = [];

  console.log(thingsManifest);

  for (var objects of thingsManifest) {
    for (var thing of objects.things) {
      let config = {
        id: thing.id,
        type: objects.type,
        networkConfig: awsIotNetworkConfig,
        options: thing.options
      };
      thingsConfig.push(config);
    }
  }

  var thingsProcesses: { [Thing]: any } = {};

  for (var thing of thingsConfig) {
    thingsProcesses[thing.id] = child_process.fork("./things/thingWorker.js");
    thingsProcesses[thing.id].send(thing);
  }
  return thingsProcesses;
}
