//@flow

import { IsValidJSON } from "../utils";
const awsIot = require("aws-iot-device-sdk");
const logger = require("../utils/logger.js");

interface NetworkConfig {
  keyPath: string; // private key path
  certPath: string; // thing certificate path
  caPath: string; // root certificate path
  host: string; // host adress
  clientId?: string; // thing unique id
}

export default class Thing {
  id: string;
  config: NetworkConfig;
  awsThing: any;
  logger: any;

  /** Object state, please instantiate a new state for each state modification
    * to detect change easly 
    example : 
    {
        on : false,
        bri:  80
    }
     */
  state: Object;

  // args are useful to setup the object like connection settings,...
  constructor(id: string, config: NetworkConfig, options: Object) {
    if (this.constructor === Thing) {
      throw new TypeError(
        'Abstract class "IotObject" cannot be instantiated directly.'
      );
    }
    this.id = id;
    this.config = Object.assign(config, { clientId: this.id });
    this.state = {};

    // logger for debug purpose
    this.logger = logger({
      level: "info",
      prettyPrint: true,
      colorize: true,
      silent: false,
      timestamp: true
    });
  }

  /** implement the different tasks of the thing
   * like listen changes, send data with time interval, subscribe,....
   * you can use node-cron to schedule tasks
   */
  run(callback: any) {
    this.connect(callback);
  }

  connect(callback: any) {
    this.awsThing = awsIot.thingShadow(this.config);
    let self = this;

    this.awsThing.on("connect", function() {
      self.awsThing.register(
        self.id,
        {
          enableVersioning: false
        },
        function() {
          self.logger.info("connection with " + self.id + " opened");
          self.awsThing.get(self.id);
          callback(null);
        }
      );
    });

    this.awsThing.on("status", function(
      thingName,
      stat,
      clientToken,
      stateObject
    ) {
      self.logger.log(
        "debug",
        "received " + stat + " on " + thingName,
        stateObject
      );
    });

    this.awsThing.on("timeout", function(thingName, clientToken) {
      self.logger.error(
        "received timeout on " + thingName + " with token: " + clientToken
      );
    });
    this.awsThing.on("error", error => {
      self.logger.info("OUPS ", error);
    });
  }

  disconnect(callback: any) {
    this.awsThing.disconnect();
    let self = this;

    this.awsThing.on("close", function() {
      self.logger.info("connection with " + self.id + " closed");
      callback(null);
    });
  }

  update(data: any) {
    var self = this;
    this.awsThing.update(this.id, {
      state: {
        reported: data
      }
    });
  }

  ack() {
    var self = this;
    this.awsThing.update(this.id, {
      state: {
        desired: null
      }
    });
  }
  onChange(callback: any) {
    let self = this;

    this.awsThing.on("delta", function(thingName, stateObject) {
      self.logger.info("delta received ", stateObject);
      callback(stateObject.state);
      self.ack();
    });
  }

  send(topic: string | string[], message: any) {
    const topicPath = Array.isArray(topic) ? topic.join("/") : topic;
    const jsonMessage = typeof message == "string" ? message : JSON.stringify(message);
    this.logger.log("debug", "sending event " + topicPath, message);
    this.awsThing.publish(topicPath, jsonMessage);
  }

  on(topic: string | string[], callback: any) {
    let self = this;
    const topicPath = Array.isArray(topic) ? topic.join("/") : topic;
    this.awsThing.subscribe(topicPath, function(err, granted) {
      if (err) {
        self.logger.error(err);
      } else {
        self.logger.log("debug", "connected on topic " + topicPath);

        self.awsThing.on("message", function(messageTopic, message) {
          if (messageTopic == topicPath) {
            if (IsValidJSON(message)) {
              callback(JSON.parse(message));
            } else {
              callback(message);
            }
          }
        });
      }
    });
  }
}
