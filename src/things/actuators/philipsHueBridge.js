//@flow
import Thing from "../thing.js";

// documentation https://github.com/sqmk/huejay
import huejay from "huejay";

/** aws's shadow cannot handle an array the same way as an object
the lights property is handle as a dictionary with number,
 */

type BridgeState = {
  name?: string,
  lights?: { [id: string]: Light }
};

type Light = {
  name?: string,
  type?: string,
  state?: LightState
};

type LightState = {
  on?: boolean,
  reachable: boolean, //non configurable
  brightness?: number, // 0-254, not bri to be compatible with huejay library
  xy?: [number, number], // CIE coordinates
  colorTemp?: number, // 153-500 mixed color temperature
  transitionTime?: number, // in seconds
  effect: "colorloop" | "none"
};

type LightGroup = {
  name: string,
  lightIds: string[]
};

type Scene = {
  name: string,
  lightIds: string[],
  action: { [lightId: string]: LightState }
};

export default class PhilipsHueBridge extends Thing {
  ip: string;
  username: string;
  bridgeClient: any;
  state: BridgeState;
  //lights: Object[]; // huejay Light Objects

  constructor(id: string, config: any, options: Object) {
    super(id, config, options);
    this.ip = options.ip;
    this.username = options.username;
    this.bridgeClient = new huejay.Client({
      host: this.ip,
      username: this.username
    });
  }

  run(callback: any) {
    const self = this;
    this.connect(() => {
      this.connectToBridge().then(() => {
        this.setGroupsAndScenesHandler();
        this.logger.info("report state : ", self.state);
        self.update(self.state);
        self.onChange(newState => {
          self.setBridgeState(newState).then(() => {
            self.update(self.state);
          });
        });
      });
    });
  }

  async connectToBridge() {
    try {
      await this.bridgeClient.bridge.isAuthenticated();
      this.logger.info("bridge " + this.id + " connected");
      await this.getBridgeState();
    } catch (e) {
      this.logger.error(
        "cannot connect or authentify to bridge" +
          this.id +
          " with \n " +
          this.ip +
          " and " +
          this.username +
          "\n " +
          e
      );
    }
  }

  async getBridgeState(): Promise<BridgeState> {
    const bridgeInfo = await this.bridgeClient.bridge.get();
    const lights = await this.bridgeClient.lights.getAll();
    const lightsState = {};
    for (let light of lights) {
      lightsState[light.id] = huejayLightToLight(light);
    }
    this.state = {
      name: bridgeInfo.name,
      lights: lightsState
    };
    return this.state;
  }

  async setBridgeState(desiredState: BridgeState): Promise<BridgeState> {
    if (desiredState.name) {
      const bridge = await this.bridgeClient.bridge.get();
      bridge.name = desiredState.name;
      await this.bridgeClient.bridge.save(bridge);
    }
    if (desiredState.lights) {
      const desiredLights = desiredState.lights;
      let lightIdx = Object.keys(desiredLights);
      for (let idx of lightIdx) {
        const desiredLight = desiredLights[idx];
        let light = await this.bridgeClient.lights.getById(idx);
        if (desiredLight.name) {
          light.name = desiredLight.name;
        }
        if (desiredLight.state) {
          if (desiredLight.state.on != undefined) {
            light.on = desiredLight.state.on;
            // the lights need to be on to modify other properties as brightness and color,
            // so need to save and update the client
            light = await this.bridgeClient.lights.save(light);
          }
          if (light.on) {
            const lightState = Object.keys(desiredLight.state);
            for (let attr of lightState) {
              console.log("attr " + attr +" desired: "+ desiredLight.state[attr])
              light[attr] = desiredLight.state[attr];
            }
          }
        }
        await this.bridgeClient.lights.save(light);
      }
    }
    return this.getBridgeState();
  }

  setGroupsAndScenesHandler() {
    const self = this;
    const DEFAULT_THING_PATH = ["things", this.id, "features"];
    this.on(DEFAULT_THING_PATH.concat(["groups", "get"]), () => {
      const result = {};
      const getRequest = self.bridgeClient.groups.getAll().then(groups => {
        for (let group of groups) {
          result[group.id] = huejayGroupToGroup(group);
        }
        return result;
      });
      handleAnswer(
        self,
        getRequest,
        DEFAULT_THING_PATH.concat(["groups", "get"])
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["groups", "set"]), desiredGroup => {
      for (let id of Object.keys(desiredGroup)) {
        const setRequest = self.bridgeClient.groups.getById(id).then(group => {
          for (let attr of desiredGroup[id].state) {
            group[attr] = desiredGroup[id].state[attr];
          }
          return self.bridgeClient.groups.save(group);
        });
        handleAnswer(
          self,
          setRequest,
          DEFAULT_THING_PATH.concat(["groups", "set"])
        );
      }
    });
    this.on(DEFAULT_THING_PATH.concat(["groups", "new"]), desiredGroup => {
      const newGroup = new self.bridgeClient.groups.Group();
      newGroup.name = desiredGroup.name;
      newGroup.lightIds = desiredGroup.lightIds;
      const newRequest = self.bridgeClient.groups.create(newGroup);
      handleAnswer(
        self,
        newRequest,
        DEFAULT_THING_PATH.concat(["groups", "new"])
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["groups", "update"]), desiredGroups => {
      for (let id of Object.keys(desiredGroups)) {
        const updateRequest = self.bridgeClient.groups
          .getById(id)
          .then(group => {
            group.name = desiredGroups[id].name;
            group.lightIds = desiredGroups[id].lightIds;
            return self.bridgeClient.groups.save(group);
          });
        handleAnswer(
          self,
          updateRequest,
          DEFAULT_THING_PATH.concat(["groups", "update"]),
          id
        );
      }
    });
    this.on(DEFAULT_THING_PATH.concat(["groups", "delete"]), groupId => {
      const deleteRequest = self.bridgeClient.groups.delete(groupId);
      handleAnswer(
        self,
        deleteRequest,
        DEFAULT_THING_PATH.concat(["groups", "delete"], groupId)
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["scenes", "get"]), () => {
      const result = {};
      const getRequest = self.bridgeClient.scenes.getAll().then(scenes => {
        for (let scene of scenes) {
          result[scene.id] = huejaySceneToScene(scene);
        }
        return result;
      });
      handleAnswer(
        self,
        getRequest,
        DEFAULT_THING_PATH.concat(["scenes", "get"])
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["scenes", "set"]), sceneId => {
      const setRequest = self.bridgeClient.scenes.recall(sceneId);
      handleAnswer(
        self,
        setRequest,
        DEFAULT_THING_PATH.concat(["scenes", "set"])
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["scenes", "new"]), desiredScene => {
      const newScene = new self.bridgeClient.scenes.Scene();
      newScene.name = desiredScene.name;
      newScene.lightIds = desiredScene.lightIds;
      newScene.recycle = false;
      newScene.captureLightState = true;

      const newRequest = self.bridgeClient.scenes
        .create(newScene);
      handleAnswer(
        self,
        newRequest,
        DEFAULT_THING_PATH.concat(["scenes", "new"])
      );
    });
    this.on(DEFAULT_THING_PATH.concat(["scenes", "update"]), desiredScene => {
      for (let id of Object.keys(desiredScene)) {
        const updateRequest = self.bridgeClient.scenes
          .getById(id)
          .then(scene => {
            scene.name = desiredScene[id].name;
            scene.lightIds = desiredScene[id].lightIds;
            const lightIds = Object.keys(desiredScene.action);
            for (let lightId of lightIds) {
              scene.setLightState(lightId, desiredScene.action[lightId]);
            }
            return self.bridgeClient.scenes.save(scene);
          });
        handleAnswer(
          self,
          updateRequest,
          DEFAULT_THING_PATH.concat(["scenes", "update"]),
          id
        );
      }
    });
    this.on(DEFAULT_THING_PATH.concat(["scenes", "delete"]), sceneId => {
      let deleteRequest = self.bridgeClient.scenes.delete(sceneId);
      handleAnswer(
        self,
        deleteRequest,
        DEFAULT_THING_PATH.concat(["scenes", "delete"]),
        sceneId
      );
    });
  }
}

function huejayLightToLight(light: any): Light {
  const lightState = huejayLightToLightState(light);
  return {
    name: light.name,
    type: light.type,
    state: lightState
  };
}

function huejayLightToLightState(light: any) {
  return {
    on: light.on,
    brightness: light.brightness,
    reachable: light.reachable,
    xy: light.xy,
    colorTemp: light.colorTemp,
    transitionTime: light.transitionTime,
    effect: light.effect
  };
}

function huejayGroupToGroup(group: any): LightGroup {
  return { name: group.name, lightIds: group.lightIds };
}

function huejaySceneToScene(scene: any): Scene {
  return {
    name: scene.name,
    lightIds: scene.lightIds
  };
}

function handleAnswer(
  sender: any,
  request: Promise<any>,
  mqttPath: string[],
  onSuccessReturnValue?: any
): Promise<any> {
  return request
    .then(obj => {
      sender.send(mqttPath.concat(["accepted"]), onSuccessReturnValue || obj);
    })
    .catch(err => {
      sender.send(mqttPath.concat(["rejected"]), err);
    });
}
