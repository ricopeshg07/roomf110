//@flow

/** Objects classes
 */
import T1 from "./sensors/t1.js";
import PhilipsHueBridge from "./actuators/philipsHueBridge.js";
import GrovePiSensors from "./sensors/grovepi.js";

/** Things dictionary
 * List the different things type and their corresponding id to use in the manifest
 * separate to
 */
const ThingsModule: { [type_name: string]: any } = {
  t1: T1,
  philipsHueBridge: PhilipsHueBridge,
  GrovePiSensors: GrovePiSensors
};

process.on("message", function(thingConfig: {
  id: string,
  type: string,
  networkConfig: any,
  options?: Object
}) {
  const thing = new ThingsModule[thingConfig.type](
    thingConfig.id,
    thingConfig.networkConfig,
    thingConfig.options
  );
  thing.run();
});
