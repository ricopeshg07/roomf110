# Backend Mind Project : augmented room

## Introduction

This repository is the backend part of the Mind Student IoT project to create an augmented room demo in room f110. The code in this directory makes the link between the connected objects and the cloud aws and has been written in Javascript with the Flow framework.

## Prerequisites

To run this project correctly you will need :

- unix system
- [npm & node](https://www.npmjs.com/get-npm) installed
- [aws account](https://aws.amazon.com/) with IoT service access
- MQTT understanding and ports opened ([MQTT with Aws](https://docs.aws.amazon.com/fr_fr/iot/latest/developerguide/protocols.html))
![](docs/images/mqttPorts.jpg)

  ### Javascript
  This project uses the latest javascript features esm so to run the code you will need to use [esm package](https://github.com/standard-things/esm) when running node:

```bash
 node -r esm index.js
```

### Flow framework

It also uses the framework [Flow](https://flow.org/) (without the babel compilator). You can get the raw Javascript in lib directory once the project compiled

### Grovepi

By default the master branch uses Grovepi code to make use of Grovepi Sensors so you will also need to install Grovepi on your system using ([more information on GrovePi](https://www.dexterindustries.com/GrovePi/get-started-with-the-grovepi/setting-software/)) :

```bash
 sudo curl -kL dexterindustries.com/update_grovepi | bash
 sudo reboot
```

## Quick Install

With node and an aws account ready you can easily install this project by first cloning this repository and running this command in the project root directory:

```bash
npm install
```

The project should have compiled and install without problems.

Fill now the network/networkConfig.json with your own aws account parameters and default certificate.
Fill the thingsManifest.json with the things you wish to run.
You will find more information about how to setup those files in the How it works section.

if everything is setup correctly you can run the start script (recompile and run the project) with the command :

```bash
npm run-script start
```

## How it works

To understand how the project works you will need some basic understandings on how [aws IoT service](https://docs.aws.amazon.com/iot/index.html) and [nodejs](https://nodejs.org/en/docs/) work.  
The project uses the MQTT protocol and so you will also need to understand it. Basically the MQTT protocol works on the publisher/subscriber principle, it means you will send json or binary data to a topic (like /my/awesome/topic) and different client will subscribe and/or publish on this topic to recover the data.
![](docs/images/simpleMQTTexplained.png)

This project runs a set of Thing instance (things/thing.js) in a new [childprocess](https://nodejs.org/api/child_process.html) and calls the run method.
The Thing implements also a logger [winston package](https://github.com/winstonjs/winston) that you can use as you wish.
The thing Object is linked to his aws IoT representation with his id and certificate. By default, it also registers to the aws shadow of your object. The shadow of your object is a service that aws IoT provides to help you to manage the state of your thing ([more information about aws shadow](https://docs.aws.amazon.com/fr_fr/iot/latest/developerguide/iot-device-shadows.html)).
On this project side, the shadow state is managed as a json object and as follow :

```json
{
  "desired": {
    // (if useful) the desired state of your object
  },
  "reported": {
    //your object state reported
  }
}
```

The shadow will create a 'delta' attribute and publish (warning continuously) any differences between the reported and desired state.

### How to setup your aws Iot Connection

To use this project you will need to setup your aws account accordingly.
You have to be able to access the aws IoT service.
You will have to setup your aws connection in the file network/networkConfig.json as follow :

```json
{
  "keyPath": "../network/cert/rasp-private.pem.key",
  "certPath": "../network/cert/rasp-certificate.pem.crt",
  "caPath": "../network/cert/CAroot.pem",
  "host": "a2925nglmmp0xz-ats.iot.eu-west-1.amazonaws.com",
  "region": "eu-west-1"
}
```

First you will have to set the 'host' endpoint attribute, you can find it in aws IoT dashboard -> parameters, the 'region' attribute depends of which region you have set up the IoT service (for example, 'eu-west-1' stands for Ireland).

Second you will have to setup the default certificates to use for each Thing Object (keyPath and certPath attributes). If you want to use different certificate for each thing it can be override in your Thing extended implementation.

Finally you will need a root certificate, you can find them with all the instructions in the aws iot documentation about certficates [here](https://docs.aws.amazon.com/fr_fr/iot/latest/developerguide/managing-device-certs.html).

### How to setup your aws Iot Service

![](docs/images/awsThingsManagementConsole.jpg)

You will need to setup your aws Iot service by creating the aws representation of your things. Warning ! The Thing id have to be unique and identical to the one you will use in the thingsManifest.json.  

![](docs/images/awsCertificateCreation.jpg)

 After you will need to attach them a certificate with the correct policies. The certificate(s) created will be the one(s) to be used to connect your things on your unix system. 

### How to setup the project

To setup a thing you will need to fill correctly the thingsManifest.json. The file represents a list of typed things with a type and a list of Thing Object as follow:

```json
[
  {
    "type": "MyTestSensor",
    "things": [
      {
        "id": "sensor1",
        "options": {}
      }
    ]
  }
]
```

The type attribute is defined in the things/thingWorker.js so to add/use things scripts you will need to add it to this file.
The id of a thing has to be the same as the one in your aws IoT thing.
The options attribute is an any Object you can use in your implemented class, it can be use to setup your Thing Object and/or differentiate your Things like adding a 'pin' attribute in your options (sensor1 -> pin 5, sensor2 -> pin 4, ...).
For example, you can setup 2 distinct sensors of same type (2 luminosity sensors,...) on a same unix system with there representation in aws cloud.

### How to add a new type of Thing

A new type of connected object extends the class Thing and implements the method run as follow :

```javascript
export default class MyNewType extends Thing {
  run(callback: any) {
    // the thing has been instantiated
    // and is currently running
    //do your stuff
    this.connect(() => {
      //once connected to aws thing
      //do your stuff
    });
  }
}
```

You can override the constructor to override the default setup (certificates, id, ...).

```javascript
constructor(id: string, config: NetworkConfig, options: Object) {
   //change stuff
   super(id, config, options);
   //do new stuff
 }
```

The Thing Class has a few useful methods to manipulate the Thing shadow and publish/subscribe to different topics like :

- 'on' to subscribe to a MQTT topic

```javascript
on(topic: string | string[], (message)=>{
   // do your stuff
});
```

- 'send' to publish in a MQTT topic

```javascript
send(topic: string | string[], message: any);
```

- 'onChange' subscribe at shadow delta changes

```javascript
onChange(() => {
  // do your staff on shadow delta received
});
```

- 'update' publish in the reported attribute of the shadow

```javascript
update(yourData);
```

every method of this class can be override to personalize his object.  
Once you created your new Thing script you will have to add it to the dictionary in thingWorker.js with his import and give it an id for his type to be used in the thingsManifest.json like this :

```javascript
import MyNewThing from "./sensors/mynewthing.js";
/** Things dictionary
 * List the different things type and their corresponding id to use in the manifest
 * separate to
 */
const ThingsModule : { [type_name: string]: Thing} = {
  /// ... my types....
   'MyNewThing': MyNewThing,
}
```

## Currently Supported things and more information

We created a MQTT topic to add new features, it has to follow this principle :

```
roomf110/features/replaceWithMyFeature/replaceWithMyTopic
```

The list of curently supported things:

- GrovePi sensors, you can find the script in src/things/sensors/grovepi.js, HOWEVER  be aware that the Grovepi board can only be access by one process at time, so you have to setup all your sensors in one process or you will risk to receive corrupt data.
- Philips Hue Bridge, you can find the script in src/things/actuators/philipsHueBridge.js, all the functionalitites are not implemented : only Groups, Scenes , Light State. You can improve this class by implementing new functionalities like entertainment functionality.  
  To setup the bridge you will need to know the bridge address ip and have an user id that you can create by sending a HTTP post request to the bridge described in the [Philips Hue documentation](https://developers.meethue.com/) (you will need to create an account).  
  If you want to get access to the bridge console (for manual modification if not it's preferable to use Philips hue App on Smartphone), you can find different tutorials on Internet explaining how to root the Philips Hue Bridge like [this one](https://blog.andreibanaru.ro/2018/03/27/philips-hue-2-1-enabling-wifi/).
- Arduino using [Johnny five library](https://github.com/rwaldron/johnny-five), we used johnny five to control the arduino in js through the raspberry, if you encounter some problems `serialport` installing this library, see the serialport [documentation](https://serialport.io/docs/en/guide-installation) explaining how to install this on raspberry.
- [node-cron](https://github.com/kelektiv/node-cron) package is installed by default, so you can use it to schedule different tasks in your Things